﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContactManager.Migrations
{
    public partial class AddAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DOB",
                table: "Contacts",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Contacts",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Street1 = table.Column<string>(nullable: true),
                    Street2 = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    Zip = table.Column<int>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    ContactId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Addresses_Contacts_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contacts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "Id", "City", "ContactId", "State", "Street1", "Street2", "Type", "Zip" },
                values: new object[,]
                {
                    { new Guid("b39467d9-a1f4-4843-aee9-7e9060448ded"), "Melvile", new Guid("930d4f10-9daf-4582-b4bb-cb9abfd382b3"), "NY", "10 Main St", "", 0, 11757 },
                    { new Guid("1632295e-fc3c-49c5-b30f-d673fc816b73"), "Westbury", new Guid("b728f6ef-65d8-4da2-8e5f-0f67e3c3401c"), "NY", "245 Coral Place", "Appt #3", 0, 11590 },
                    { new Guid("a0762d8b-9dc6-4fa0-99dc-6f7438732760"), "Los Angles", new Guid("b728f6ef-65d8-4da2-8e5f-0f67e3c3401c"), "CA", "1 Apple Way", "", 1, 11757 },
                    { new Guid("7931505c-fca8-4a35-8d24-b32df341643d"), "Melvile", new Guid("99580d68-9d2f-4552-862e-06b3204193f1"), "NY", "10 Main St", "", 0, 11757 }
                });

            migrationBuilder.UpdateData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: new Guid("930d4f10-9daf-4582-b4bb-cb9abfd382b3"),
                columns: new[] { "DOB", "Title" },
                values: new object[] { new DateTime(1960, 5, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mr." });

            migrationBuilder.UpdateData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: new Guid("99580d68-9d2f-4552-862e-06b3204193f1"),
                columns: new[] { "DOB", "Title" },
                values: new object[] { new DateTime(1980, 1, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mr." });

            migrationBuilder.UpdateData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: new Guid("b728f6ef-65d8-4da2-8e5f-0f67e3c3401c"),
                columns: new[] { "DOB", "Title" },
                values: new object[] { new DateTime(1950, 9, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mr." });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_ContactId",
                table: "Addresses",
                column: "ContactId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropColumn(
                name: "DOB",
                table: "Contacts");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Contacts");
        }
    }
}
