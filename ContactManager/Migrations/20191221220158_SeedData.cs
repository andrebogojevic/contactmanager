﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ContactManager.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Contacts",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[] { new Guid("930d4f10-9daf-4582-b4bb-cb9abfd382b3"), "Bill", "Gates" });

            migrationBuilder.InsertData(
                table: "Contacts",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[] { new Guid("b728f6ef-65d8-4da2-8e5f-0f67e3c3401c"), "Steve", "Jobs" });

            migrationBuilder.InsertData(
                table: "Contacts",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[] { new Guid("99580d68-9d2f-4552-862e-06b3204193f1"), "Sundar", "Pichai" });

            migrationBuilder.InsertData(
                table: "EmailAddresses",
                columns: new[] { "Id", "ContactId", "Email", "Type" },
                values: new object[,]
                {
                    { new Guid("5111f412-a7f4-4169-bb27-632687569ccd"), new Guid("930d4f10-9daf-4582-b4bb-cb9abfd382b3"), "Bill@gates.com", 0 },
                    { new Guid("3ddeb084-5e5d-4eca-b275-e4f6886e04dc"), new Guid("b728f6ef-65d8-4da2-8e5f-0f67e3c3401c"), "Steve@Jobs.com", 0 },
                    { new Guid("3a406f64-ad7b-4098-ab01-7e93aae2b851"), new Guid("b728f6ef-65d8-4da2-8e5f-0f67e3c3401c"), "SteveJobs@apple.com", 1 },
                    { new Guid("d1a50413-20c0-4972-a351-8be24e1fc939"), new Guid("99580d68-9d2f-4552-862e-06b3204193f1"), "SundarPichai@gmail.com", 1 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "EmailAddresses",
                keyColumn: "Id",
                keyValue: new Guid("3a406f64-ad7b-4098-ab01-7e93aae2b851"));

            migrationBuilder.DeleteData(
                table: "EmailAddresses",
                keyColumn: "Id",
                keyValue: new Guid("3ddeb084-5e5d-4eca-b275-e4f6886e04dc"));

            migrationBuilder.DeleteData(
                table: "EmailAddresses",
                keyColumn: "Id",
                keyValue: new Guid("5111f412-a7f4-4169-bb27-632687569ccd"));

            migrationBuilder.DeleteData(
                table: "EmailAddresses",
                keyColumn: "Id",
                keyValue: new Guid("d1a50413-20c0-4972-a351-8be24e1fc939"));

            migrationBuilder.DeleteData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: new Guid("930d4f10-9daf-4582-b4bb-cb9abfd382b3"));

            migrationBuilder.DeleteData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: new Guid("99580d68-9d2f-4552-862e-06b3204193f1"));

            migrationBuilder.DeleteData(
                table: "Contacts",
                keyColumn: "Id",
                keyValue: new Guid("b728f6ef-65d8-4da2-8e5f-0f67e3c3401c"));
        }
    }
}
